#http://docs.python-requests.org/en/master/user/quickstart/#more-complicated-post-requests
#https://github.com/requests/requests-oauthlib
#https://dev.twitter.com/rest/media/uploading-media
#http://docs.python-guide.org/en/latest/scenarios/json/
#http://stackoverflow.com/questions/29104107/upload-image-using-post-form-data-in-python-requests

#@medstweet
#id 764024226017742848

#database is a list of dicts. dicts have entries:
	#name: "name"
	#user: "user_ID"
	#times: [times]

#Get all the messages since the last message ID
#parse/reply to them all
	#parse - parseMessage
	#update database - updateDatabase
	#reply - sendMessageByID
#make list of reminders to be sent - checkMed
#send them - sendMessageByID

#https://dataset.readthedocs.io/en/latest/
# lazy database thing that might work

#find some place to upload/run this
#https://devcenter.heroku.com/articles/python-pip

import sys
import io
import requests
import json
import dbm
import pickle
import time

from requests_oauthlib import OAuth1
from twisted.internet import task
from twisted.internet import reactor

def sendMessageByName(auth, screen_name, text):
	"""
	#this works!
	url='https://api.twitter.com/1.1/direct_messages/new.json'
	screen_name='screen_name'
	text='test'
	params = {'screen_name': screen_name, 'text': text}
	response = requests.post(url=url, auth=auth, params=params)
	"""
	url = 'https://api.twitter.com/1.1/direct_messages/new.json'
	params = {'screen_name': screen_name, 'text': text}
	response = requests.post(url=url, auth=auth, params=params)
	return response

def sendMessageByID(auth, user_id, text):
	url = 'https://api.twitter.com/1.1/direct_messages/new.json'
	params = {'user_id': user_id, 'text': text}
	response = requests.post(url=url, auth=auth, params=params)
	return response

def getMessages(auth, since_id='0', skip_status='true', include_entities='false'):
	url = 'https://api.twitter.com/1.1/direct_messages.json'
	params = {'since_id': since_id, 'skip_status': skip_status,'include_entities': include_entities}
	response = requests.get(url=url, auth=auth, params=params)
	return response

def sendPost(auth, url, params):
	response = requests.post(url=url, auth=auth, params=params)
	return response

def sendGet(auth, url, params):
	response = requests.get(url=url, auth=auth, params=params)
	return response

def postStatus(auth, status, in_reply_to_status_id='0'):
	"""
	#post a status
	#DO THIS ONLY ONCE PLS
	url = 'https://api.twitter.com/1.1/statuses/update.json'
	status = '@thecasserpillar proof'
	params = {'status': status, 'in_reply_to_status_id': 
	'id_number', 'media_ids': img_id}
	response = requests.post(url, auth=auth, params=params)
	"""
	url = 'https://api.twitter.com/1.1/statuses/update.json'
	params = {'status': status, 'in_reply_to_status_id': in_reply_to_status_id}
	response = requests.post(url, auth=auth, params=params)
	return response

def parseMessage(userID, messageText):
	"""
	Takes in a string received in a twitter message
	Parses it into a dictionary containing the relevant info
	Message types:
		medName medTime
		medName startTime endTime periodTime
		null
	"""
	err = 0
	words = messageText.split()
	newMed = {}
	if len(words) == 2 and int(words[1]):
		newMed['name'] = words[0]
		newMed['user'] = userID
		newMed['times'] = [int(words[1])]
	elif len(words) == 4:
		newMed['name'] = words[0]
		newMed['user'] = userID
		startTime = int(words[1])
		endTime = int(words[2])
		period = int(words[3])
		medTimes = list(range(startTime, endTime, period))
		newMed['times'] = medTimes
	return newMed

def updateDatabase(newMed, database):
	if newMed != {}:
		database.append(newMed)
	return database

def is_number(s):
	#http://pythoncentral.io/how-to-check-if-a-string-is-a-number-in-python-including-unicode/
	try: 
		float(s)
		return True
	except ValueError:
		return False

def checkMed(medication, previousTime, currentTime):
	if previousTime > currentTime:
			currentTime = currentTime + 2400
	for time in medication['times']:
		if (time > previousTime) and (time <= currentTime):
			sendMessage = True
		else:
			sendMessage = False
	return sendMessage

def onWake():
	#store the message since_id, the previous waketime in minutes in 24h time
	#and the database
	parameters = pickle.load(open('database.dump', 'rb'))
	database = parameters['database']
	previousTime = parameters['previousTime']
	since_id = parameters['since_id']
	currentTime = str((time.localtime().tm_hour*100 + time.localtime().tm_min))
	response = getMessages(auth, since_id=since_id)
	messages = json.loads(response.text)
	for message in messages:
		message_text = message['text']
		user_id = message['sender_id_str']
		message_id = message['id_str']
		newMed = parseMessage(user_id, message_text)
		database = updateDatabase(newMed, database)
		sendMessageByID(auth, user_id, "Thanks! I've added your medication to the database!\n\n" + str(newMed))
	for med in database:
		if checkMed(med, int(previousTime), int(currentTime)):
			sendMessageByID(auth, med['user'], "You should take your " + med['name'] + ' now.')

	parameters['previousTime'] = currentTime
	parameters['since_id'] = messages[0]['id_str']
	parameters['database'] = database
	pickle.dump(parameters, open('database.dump', 'wb'))
	pass

sys.stdout = io.TextIOWrapper(sys.stdout.detach(), sys.stdout.encoding, 'backslashreplace')

#Add your keys here
medstweet_key = 'key'
medstweet_secret = 'secret_key'
meds_token = 'token'
meds_secret = 'account_secret'

auth = OAuth1(medstweet_key, medstweet_secret, meds_token, meds_secret)

parameters = {'database': [], 'since_id': "0", 'previousTime': "0"}
pickle.dump(parameters, open('database.dump', 'wb'))

timeout = 300.0
l = task.LoopingCall(onWake)
l.start(timeout)
reactor.run()
